import logging

from json.decoder import JSONDecodeError
from requests import Session
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from data_structures.datacenter import Datacenter

URL = "http://www.mocky.io/v2/5e539b332e00007c002dacbe"


def get_data(url, max_retries=5, delay_between_retries=1):
    """
    Fetch the data from http://www.mocky.io/v2/5e539b332e00007c002dacbe
    and return it as a JSON object.

    Args:
        url (str): The url to be fetched.
        max_retries (int): Number of retries.
        delay_between_retries (int): Delay between retries in seconds.
    Returns:
        data (dict)
    """
    session = Session()
    retry = Retry(
        total=max_retries,
        read=max_retries,
        connect=max_retries,
        backoff_factor=delay_between_retries,
        status_forcelist=(500, 502, 504),
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    logging.info('Getting data from cloud')
    rc = session.get(url)
    # Check for the response to be a JSON
    try:
        return rc.json()
    except JSONDecodeError:
        return {}


def main():
    """
    Main entry to our program.
    """
    data = get_data(URL)

    if not data:
        raise ValueError('No data to process')

    datacenters = [
        Datacenter(key, value)
        for key, value in data.items()
    ]

    for dc in datacenters:
        print(dc.name)
        for c in dc.cluster_list:
            print(c.name)
            for n in c.network_list:
                print(n.ipv4_network)
                for e in n.entries:
                    print(e.address)


if __name__ == '__main__':
    main()
