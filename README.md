# SDN



## Getting started

You need the `requests` library to run this script, so before trying to run it, install the libraries from the requirements file:

```pip install -r requirements.txt```

## Running the script

```python main.py```

## Running the tests

```python -m unittest discover -s tests```

