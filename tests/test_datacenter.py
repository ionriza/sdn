import unittest
from data_structures.datacenter import Datacenter


class TestDataCenter(unittest.TestCase):
    def test_add_valid_data_center(self):
        d = Datacenter(name='Bucharest', cluster_list={'BUC-1': {}})
        self.assertEqual(len(d.cluster_list), 1)

    def test_add_invalid_data_center(self):
        d = Datacenter(name='Budapest', cluster_list={'BUC-1': {}})
        self.assertEqual(len(d.cluster_list), 0)
