import unittest

from data_structures.network_collection import NetworkCollection


class TestNetworkCollection(unittest.TestCase):
    def test_add_valid_network_collection(self):
        raw_entry_list = [{
            'address': '192.168.0.1',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        }]
        network = NetworkCollection(ipv4_network='192.168.0.0/24', raw_entry_list=raw_entry_list)
        self.assertEqual(len(network.entries), 1)

    def test_add_invalid_network_collection(self):
        raw_entry_list = [{
            'address': '192.168.0.256',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        }]
        network = NetworkCollection(ipv4_network='192.168.0.0/24', raw_entry_list=raw_entry_list)
        self.assertEqual(len(network.entries), 0)

    def test_add_invalid_network_subnet_network_collection(self):
        raw_entry_list = [{
            'address': '192.168.1.1',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        }]
        network = NetworkCollection(ipv4_network='192.168.0.0/24', raw_entry_list=raw_entry_list)
        self.assertEqual(len(network.entries), 0)

    def test_sorted_records(self):
        raw_entry_list = [{
            'address': '192.168.0.10',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        },
        {
            'address': '192.168.0.1',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        },
        {
            'address': '192.168.1.1',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        },
        {
            'address': '192.168.0.15',
            'available': True,
            'last_used': '01/01/70 00:00:00'
        }]
        network = NetworkCollection(ipv4_network='192.168.0.0/23', raw_entry_list=raw_entry_list)
        self.assertEqual([str(e) for e in network.entries],
                         ['192.168.0.1', '192.168.0.10', '192.168.0.15', '192.168.1.1'])
