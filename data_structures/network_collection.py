from data_structures import utils
from ipaddress import ip_network

from data_structures.entry import Entry


class NetworkCollection:
    def __init__(self, ipv4_network, raw_entry_list):
        """
        Constructor for NetworkCollection data structure.

        self.ipv4_network -> ipaddress.IPv4Network
        self.entries -> list(Entry)
        """
        self.ipv4_network = ip_network(ipv4_network)
        self.entries = [
            Entry(entry.get('address', ''), entry.get('available', False), entry.get('last_used', '01/01/70 00:00:00'))
            for entry in raw_entry_list]
        self.remove_invalid_records()
        self.sort_records()

    def __repr__(self):
        return f'{self.ipv4_network}:[{",".join([c.address for c in self.entries])}]'

    def remove_invalid_records(self):
        """
        Removes invalid objects from the entries list.
        """

        self.entries = list(filter(lambda c: all([
            utils.check_ip_valid_addresses(c.address) and utils.check_ip_within_network(c.address, self.ipv4_network)]),
                                          self.entries))

    def sort_records(self):
        """
        Sorts the list of associated entries in ascending order.
        DO NOT change this method, make the changes in entry.py :)
        """

        self.entries = sorted(self.entries)
