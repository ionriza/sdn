import ipaddress


def check_ip_valid_addresses(ip_address_str):
    try:
        ipaddress.ip_address(ip_address_str)
    except ValueError:
        return False
    else:
        return True


def check_ip_within_network(ip_address_str, ip_network):
    return ipaddress.ip_address(ip_address_str) in ip_network
