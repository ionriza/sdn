from datetime import datetime
from ipaddress import ip_address


class Entry:
    def __init__(self, address, available, last_used):
        """
        Constructor for Entry data structure.

        self.address -> str
        self.available -> bool
        self.last_used -> datetime
        """
        self.address = address
        self.available = available
        self.last_used = datetime.strptime(last_used, '%d/%m/%y %H:%M:%S')

    def __repr__(self):
        return f'{self.address}'

    def __lt__(self, other):
        return ip_address(self.address) < ip_address(other.address)
