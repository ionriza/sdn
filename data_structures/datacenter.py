import re

from data_structures.cluster import Cluster


class Datacenter:
    def __init__(self, name, cluster_list):
        """
        Constructor for Datacenter data structure.

        self.name -> str
        self.clusters -> list(Cluster)
        """
        self.name = name

        self.cluster_list = [
            Cluster(cluster_name, cluster_properties.get('networks', {}), cluster_properties.get('security_level', 0))
            for cluster_name, cluster_properties in cluster_list.items()]

        self.remove_invalid_clusters()


    def __repr__(self):
        return f'{self.name} - {len(self.cluster_list)} clusters'

    def remove_invalid_clusters(self):
        """
        Removes invalid objects from the clusters list.
        """
        self.cluster_list = list(
            filter(lambda c: re.match('^%s-([1-9][0-9]{0,2})$' % self.name[0:3].upper(), c.name), self.cluster_list))