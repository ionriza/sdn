from data_structures.network_collection import NetworkCollection


class Cluster:
    def __init__(self, name, network_list, security_level):
        """
        Constructor for Cluster data structure.

        self.name -> str
        self.security_level -> int
        self.networks -> list(NetworkCollection)
        """
        self.name = name
        self.network_list = [
            NetworkCollection(ipv4_network, networks) for ipv4_network, networks in network_list.items()]
        self.security_level = security_level

    def __repr__(self):
        return f'{self.name}: {len(self.network_list)} networks, security level {self.security_level}'